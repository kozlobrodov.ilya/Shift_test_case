# Salary verification system

## О проекте

Сервис просмотра текущей зарплаты и даты следующего повышения. Каждый сотрудник может видеть только свою сумму.

## Stack:

- Python 3.10
- FastAPI
- SQLAlchemy
- PostgreSQL
- Docker

## Запуск проекта:

- убедитесь, что у вас установлен **Docker**
- в каталоге проекта создайте .env файл с параметрами виртуального окружения по примеру .env.example
- создайте и запустите Docker контейнеры:
    - ```> docker compose build ```
    - ```> docker compose up```
- в случае успешного развертывания, приложение доступно на локальном адресе http://localhost:8080
- документация приложения можно найти http://localhost:8080/docs

## Запуск тестов:

- убедитесь, что у вас установлен **Docker**
- создайте и запустите Docker контейнеры:
    - ```>  docker compose -f tests/docker-compose.tests.yaml build ```
    - ```>  docker compose -f tests/docker-compose.tests.yaml up ```
- в случае успешного развертывания, будут запущено тестирование приложения.
